{ pkgs ? import <nixpkgs> { }, poetry2nix ? pkgs.poetry2nix, lib ? pkgs.lib, ...
}:

poetry2nix.mkPoetryApplication {
  src = lib.cleanSource ./.;
  pyproject = ./pyproject.toml;
  poetrylock = ./poetry.lock;
  python = pkgs.python3;

  doCheck = false;
}
