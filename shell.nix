{ pkgs ? import <nixpkgs> { }, mkShell ? pkgs.mkShell
, callPackage ? pkgs.callPackage }:

mkShell {
  nativeBuildInputs = with pkgs; [ git niv lefthook poetry ];
  buildInputs = [ (callPackage (import ./.) { }) ];
}
